import java.util.List;
import java.util.ArrayList;

public class Frequency {

	private List<Integer> frequencies, cumulativeFrequencies, idealizedFrequencies;

	public Frequency(List<Integer> frequencies) {
		this.frequencies = frequencies;
	}

	public List<Integer> getCumulativeFrequencies(List<Integer> frequencies) {
		List<Integer> cumulativeFrequencies = new ArrayList<>();
		int previousFrequency;

		for (Integer frequency: frequencies) {
			if (cumulativeFrequencies.isEmpty()) {
				cumulativeFrequencies.add(frequencies.get(0));
			} else {
				previousFrequency = cumulativeFrequencies.get(cumulativeFrequencies.size()-1);
				cumulativeFrequencies.add(previousFrequency + frequency);
			}
		}

		return cumulativeFrequencies;
	}

	public List<Integer> getCumulativeFrequencies() {
		cumulativeFrequencies = new ArrayList<>();
		cumulativeFrequencies.addAll(getCumulativeFrequencies(frequencies));
		return cumulativeFrequencies;
	}

	public List<Integer> getIdealizedFrequencies() {
		if (cumulativeFrequencies == null) {
			getCumulativeFrequencies();
		}

		int maxCumulativeFrequency = cumulativeFrequencies.get(cumulativeFrequencies.size()-1);
		int idealFrequency = maxCumulativeFrequency/cumulativeFrequencies.size();
		idealizedFrequencies = new ArrayList<Integer>();

		for (int i=0;i<cumulativeFrequencies.size()-1;i++) {
			idealizedFrequencies.add(idealFrequency);
		}

		int remainingFrequency = maxCumulativeFrequency - (idealFrequency*cumulativeFrequencies.size());
		idealizedFrequencies.add(idealFrequency + remainingFrequency);
		
		return idealizedFrequencies;
	}
}
