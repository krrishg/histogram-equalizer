import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import java.util.List;
import java.util.ArrayList;

public class TestFrequency {
	
	List<Integer> frequencies;
	Frequency frequency;

	@Before
	public void before() {
		frequencies =  new ArrayList<Integer>();
		frequencies.add(0);
		frequencies.add(0);
		frequencies.add(0);
		frequencies.add(6);
		frequencies.add(14);
		frequencies.add(5);
		frequencies.add(0);
		frequencies.add(0);
	
		frequency = new Frequency(frequencies);
	}

	@Test
	public void testGetCumulativeFrequencies() {
		List<Integer> cumulativeFrequencies = new ArrayList<>();
		cumulativeFrequencies.addAll(frequency.getCumulativeFrequencies(frequencies));

		assertTrue(cumulativeFrequencies.size()==8);
		assertTrue(cumulativeFrequencies.get(0)==0);
		assertTrue(cumulativeFrequencies.get(1)==0);
		assertTrue(cumulativeFrequencies.get(2)==0);
		assertTrue(cumulativeFrequencies.get(3)==6);
		assertTrue(cumulativeFrequencies.get(4)==20);
		assertTrue(cumulativeFrequencies.get(5)==25);
		assertTrue(cumulativeFrequencies.get(6)==25);
		assertTrue(cumulativeFrequencies.get(7)==25);
	}

	@Test
	public void testGetIdealizedFrequencies() {
		List<Integer> idealizedFrequencies = new ArrayList<>();
		idealizedFrequencies.addAll(frequency.getIdealizedFrequencies());
		
		assertTrue(idealizedFrequencies.size()==8);
		assertTrue(idealizedFrequencies.get(0)==3);
		assertTrue(idealizedFrequencies.get(1)==3);
		assertTrue(idealizedFrequencies.get(2)==3);
		assertTrue(idealizedFrequencies.get(3)==3);
		assertTrue(idealizedFrequencies.get(4)==3);
		assertTrue(idealizedFrequencies.get(5)==3);
		assertTrue(idealizedFrequencies.get(6)==3);
		assertTrue(idealizedFrequencies.get(7)==4);
	}
}
